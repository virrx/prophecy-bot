package utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class GetConfig {

    private String botToken, ownerID, riotAPI;

    public void GetConfig(){
        List<String> config = null;
        try {
            config = Files.readAllLines(Paths.get("src/utils/config.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        botToken = config.get(0).replaceAll("Bot Token: ", "");
        ownerID = config.get(1).replaceAll("Owner ID: ", "");
        riotAPI = config.get(2).replaceAll("Riot API Key: ", "");
    }

    public String getBotToken() {
        return botToken;
    }

    public String getOwnerID() {
        return ownerID;
    }

    public String getRiotAPI(){
        return riotAPI;
    }
}