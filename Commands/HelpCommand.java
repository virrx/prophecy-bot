package Commands;

import com.jagrosh.jdautilities.commandclient.Command;
import com.jagrosh.jdautilities.commandclient.CommandEvent;
import net.dv8tion.jda.core.EmbedBuilder;


import java.time.Instant;

/**
 * Created by Viktor on 2017-09-16.
 */
public class HelpCommand extends Command {

    public HelpCommand() {
        this.name = "help";
        this.help = "Shows help dialogue box";
        this.guildOnly = false;
    }

    @Override
    protected void execute(CommandEvent event) {
        EmbedBuilder eb = new EmbedBuilder();



        eb.setColor(event.getMember().getColor()) //info box
                .setAuthor("Help", null, event.getJDA().getSelfUser().getAvatarUrl())
                .addField("+rank", "unranked, bronze, silver, gold, platinum, diamond, master, challenger", false)
                .addField("+role", "top, jungle, mid, adc, support, fill, 1v1, inhouse", false)
                .setFooter("Prophecy Cup", null)
                .setTimestamp(Instant.now());
        event.getTextChannel().sendMessage(eb.build()).queue();
    }
}