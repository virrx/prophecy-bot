package Commands;

import com.jagrosh.jdautilities.commandclient.Command;
import com.jagrosh.jdautilities.commandclient.CommandEvent;
import net.dv8tion.jda.core.EmbedBuilder;

import net.rithms.riot.api.ApiConfig;
import net.rithms.riot.api.RiotApi;
import net.rithms.riot.api.RiotApiException;
import net.rithms.riot.api.endpoints.champion_mastery.dto.ChampionMastery;
import net.rithms.riot.api.endpoints.league.dto.LeagueList;

import net.rithms.riot.api.endpoints.static_data.dto.ChampionList;
import net.rithms.riot.api.endpoints.summoner.dto.Summoner;
import net.rithms.riot.constant.Platform;
import utils.GetConfig;


import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

/*
TODO

Finish ranked stats, normal stats and mastery
add match history, most played champions


 */

public class LeagueCommand extends Command {

    public LeagueCommand() {
        this.name = "summoner";
        this.help = "LoL summoner info";
        this.arguments = "<summoner>, \"region\" (Defaults to NA)";
        this.guildOnly = false;
    }


    @Override
    protected void execute(CommandEvent event) {

        String[] args = event.getArgs().split(", +");
        GetConfig config = new GetConfig(); //loads config file with constants.
        config.GetConfig();
        ApiConfig cfg = new ApiConfig().setKey(config.getRiotAPI());
        RiotApi api = new RiotApi(cfg);
        EmbedBuilder eb = new EmbedBuilder();

        Summoner summoner;
        Platform plat = Platform.NA;





        if (event.getArgs().isEmpty()){
            eb.setColor(event.getMember().getColor()) //info box
                    .setAuthor("Summoner Help", null, event.getJDA().getSelfUser().getAvatarUrl())
                    .addField("+summoner", "summoner name, \"region\" (Optional, Defaults to NA)", false)
                    .setFooter("Prophecy Cup", null)
                    .setTimestamp(Instant.now());
            event.getTextChannel().sendMessage(eb.build()).queue();
            return;
        }

        if (args.length > 1){ //get region, defaults to NA if empty
            plat = Platform.getPlatformByName(args[1]);
        }
        try{
            summoner = api.getSummonerByName(plat, args[0]);
        } catch(RiotApiException e) {
            event.replyError(event.getMember().getAsMention() + ", Invalid Summoner Name!");
            e.printStackTrace();
            return;
        }


        //time stuff
        LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochMilli(summoner.getRevisionDate()), ZoneId.of("-05:00"));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY, MMMM d, h:mm a", Locale.US);
        String formattedDate = date.format(formatter);




        eb.setColor(event.getMember().getColor()) //info box
                .setAuthor("Summoner Information for \"" + summoner.getName() + "\" (" + plat + ")", null, event.getJDA().getSelfUser().getAvatarUrl())
                .setThumbnail("http://ddragon.leagueoflegends.com/cdn/7.20.2/img/profileicon/" + summoner.getProfileIconId() + ".png")

                .addField("General", "**Summoner ID:** " + summoner.getId()
                        + "\n**Account ID:** " + summoner.getAccountId()
                        + "\n**Summoner Level:** " + summoner.getSummonerLevel()
                        + "\n**Last Activity:** " + formattedDate + " CST", false)

                .addField("Ranked Stats", "**Solo/Duo:** "
                        + "\n**Flex 5v5:** "
                        + "\n**Flex 3v3:** ", true)

                .addField("Normal Stats", "**Summoners Rift:** "
                        + "\n**Twisted Treeline:** "
                        + "\n**Howling Abyss:** ", true)

                .addField("Mastery", "**Total Mastery Rank:** "
                        + "\n**#1:** "
                        + "\n**#2:** "
                        + "\n**#3:** " , false)

                .setFooter("Prophecy Cup", null)
                .setTimestamp(Instant.now());
        event.getTextChannel().sendMessage(eb.build()).queue();

    }
}