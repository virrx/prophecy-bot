package Commands;

import com.jagrosh.jdautilities.commandclient.Command;
import com.jagrosh.jdautilities.commandclient.CommandEvent;
import net.dv8tion.jda.core.EmbedBuilder;
import net.rithms.riot.api.ApiConfig;
import net.rithms.riot.api.RiotApi;
import net.rithms.riot.api.endpoints.lol_status.dto.Service;

import net.rithms.riot.api.endpoints.static_data.dto.Champion;
import net.rithms.riot.api.endpoints.static_data.dto.ChampionList;
import utils.GetConfig;

import java.time.Instant;
import java.util.List;

public class LeagueChamp extends Command {

    public LeagueChamp() {
        this.name = "champ";
        this.help = "gets champ info";
        this.guildOnly = false;
    }

    @Override
    protected void execute(CommandEvent event) {
        String[] args = event.getArgs().split(", +");
        EmbedBuilder eb = new EmbedBuilder();
        GetConfig config = new GetConfig();
        config.GetConfig();
        ApiConfig cfg = new ApiConfig().setKey(config.getRiotAPI());
        RiotApi api = new RiotApi(cfg);
        //ChampionList championList =

        if (event.getArgs().isEmpty()){
            eb.setColor(event.getMember().getColor()) //info box
                    .setAuthor("Summoner Help", null, event.getJDA().getSelfUser().getAvatarUrl())
                    .addField("+summoner", "summoner name, \"region\" (Optional, Defaults to NA)", false)
                    .setFooter("Prophecy Cup", null)
                    .setTimestamp(Instant.now());
            event.getTextChannel().sendMessage(eb.build()).queue();
            return;
        }

        if (args.length > 1){ //get region, defaults to NA if empty
            //plat = Platform.getPlatformByName(args[1]);

        }

    }
}