package Commands;

import com.jagrosh.jdautilities.commandclient.Command;
import com.jagrosh.jdautilities.commandclient.CommandEvent;
import net.dv8tion.jda.core.EmbedBuilder;
import net.rithms.riot.api.ApiConfig;
import net.rithms.riot.api.RiotApi;
import net.rithms.riot.api.RiotApiException;
import net.rithms.riot.api.endpoints.lol_status.dto.Incident;
import net.rithms.riot.api.endpoints.lol_status.dto.Message;
import net.rithms.riot.api.endpoints.lol_status.dto.Service;
import net.rithms.riot.api.endpoints.lol_status.dto.ShardStatus;
import net.rithms.riot.api.endpoints.lol_status.methods.GetShardData;
import net.rithms.riot.constant.Platform;
import utils.GetConfig;

import java.time.Instant;
import java.util.List;


public class LeagueStatus extends Command {

    public LeagueStatus() {
        this.name = "status";
        this.help = "Shows status for chosen server";
        this.guildOnly = false;
    }

    @Override
    protected void execute(CommandEvent event) {
        String[] args = event.getArgs().split("\\s+"); //doesnt split properly
        EmbedBuilder eb = new EmbedBuilder();
        GetConfig config = new GetConfig();
        config.GetConfig();
        ApiConfig cfg = new ApiConfig().setKey(config.getRiotAPI());
        RiotApi api = new RiotApi(cfg);
        Platform plat = Platform.NA;

        ShardStatus shardStatus;
        Service service = new Service();
        Incident incident = new Incident();
        Message message = new Message();

        if (args.length > 1){ //get region, defaults to NA if empty
            plat = Platform.getPlatformByName(args[0]);
        }

        System.out.println(args.length);
        System.out.println(args[0]);

        try {
            shardStatus = api.getShardData(plat);
        } catch (RiotApiException e){
            event.replyError(event.getMember().getAsMention() + ", Invalid Region!");
            e.printStackTrace();
            return;
        }

        eb.setColor(event.getMember().getColor()) //info box
                .setAuthor("Server status for " + shardStatus.getName() + " (" + shardStatus.getRegionTag() + ") ", null, event.getJDA().getSelfUser().getAvatarUrl())
                .addField("Server", shardStatus.getHostname() + "\n"
                        + shardStatus.getServices().get(1) + "\n"
                        + shardStatus.getSlug(),  false)
                .setFooter("Prophecy Cup", null)
                .setTimestamp(Instant.now());
        event.getTextChannel().sendMessage(eb.build()).queue();

    }
}