import javax.security.auth.login.LoginException;
import java.io.IOException;

import Commands.*;
import com.jagrosh.jdautilities.commandclient.CommandClientBuilder;
import com.jagrosh.jdautilities.commandclient.examples.PingCommand;
import com.jagrosh.jdautilities.waiter.EventWaiter;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import net.rithms.riot.api.RiotApiException;
import utils.GetConfig;
import utils.SetPlaying;

public class Bot {
    public static JDA jda;
    public static void main(String[] args) throws LoginException, RateLimitedException, InterruptedException, IOException, RiotApiException {


        CommandClientBuilder client = new CommandClientBuilder(); //command client
        EventWaiter waiter = new EventWaiter(); //event waiter
        SetPlaying playing = new SetPlaying(); //looping "Playing" message
        GetConfig config = new GetConfig(); //loads config file with constants.
        config.GetConfig();


        client.setOwnerId(config.getOwnerID());
        client.setPrefix("+");
        client.setPlaying("Loaded!");

        client.addCommands(
                new PingCommand(),
                new RankCommand(),
                new RoleCommand(),
                new HelpCommand(),
                new LeagueCommand(),
                new LeagueStatus());

        jda = new JDABuilder(AccountType.BOT)
                .setToken(config.getBotToken())
                .setStatus(OnlineStatus.DO_NOT_DISTURB)
                .setGame(Game.of("loading..."))
                .addEventListener(waiter)
                .addEventListener(playing)
                .addEventListener(client.build())
                .buildAsync();
    }

}

